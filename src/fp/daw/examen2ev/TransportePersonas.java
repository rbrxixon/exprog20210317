package fp.daw.examen2ev;

public abstract class TransportePersonas extends Vehiculo {

	private int plazas;
	

	public TransportePersonas(String matricula, int plazas) {
		super(matricula);
		this.plazas = plazas;
	}

	public int getPlazas() {
		return plazas;
	}

	public void setPlazas(int plazas) {
		this.plazas = plazas;
	}
	
}
