package fp.daw.examen2ev;

public class Microbus extends TransportePersonas implements IPagable {

	public Microbus(String matricula, int plazas) {
		super(matricula, plazas);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getPrecioAlquiler(int dias) {
		// TODO Auto-generated method stub
		return (dias * (this.getPrecioBase() + ( (this.getPlazas() * 2) + (dias * 2) ) ) );
	}

	@Override
	public String toString() {
		return "Microbus [getPlazas()=" + getPlazas() + ", getMatricula()=" + getMatricula() + ", getPrecioBase()="
				+ getPrecioBase() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + "]";
	}
	
}
