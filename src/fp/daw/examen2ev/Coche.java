package fp.daw.examen2ev;

public class Coche extends TransportePersonas implements IPagable {
	
	public Coche(String matricula, int plazas) {
		super(matricula, plazas);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getPrecioAlquiler(int dias) {
		// TODO Auto-generated method stub
		return (dias * (this.getPrecioBase() + (this.getPlazas() * 1.5) ) );
	}

	@Override
	public String toString() {
		return "Coche [getPlazas()=" + getPlazas() + ", getMatricula()=" + getMatricula() + ", getPrecioBase()="
				+ getPrecioBase() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + "]";
	}

}
