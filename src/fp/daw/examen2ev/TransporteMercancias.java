package fp.daw.examen2ev;

public abstract class TransporteMercancias extends Vehiculo implements IPagable {

	private int pma;

	
	public TransporteMercancias(String matricula, int pma) {
		super(matricula);
		this.pma = pma;
	}

	public int getPma() {
		return pma;
	}

	public void setPma(int pma) {
		this.pma = pma;
	}
	
	@Override
	public double getPrecioAlquiler(int dias) {
		// TODO Auto-generated method stub
		return (dias * (this.getPrecioBase() + (this.getPma() * 20) ) );
	}
	
}
