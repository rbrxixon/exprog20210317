package fp.daw.examen2ev;

public interface IPagable {

	public abstract double getPrecioAlquiler(int dias);
}
