package fp.daw.examen2ev;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Programa {
	
	private ArrayList<Vehiculo> listaVehiculos;
	
	
	private Programa() {
		this.listaVehiculos = new ArrayList<>();
	}

	private ArrayList<Vehiculo> getListaVehiculos() {
		return listaVehiculos;
	}

	private void crearVehiculos() {
		Vehiculo v1 = new Coche("0001BCP", 5);
		Vehiculo v2 = new Microbus("110CDR", 15);
		Vehiculo v3 = new Furgoneta("5123TPR", 2000);
		Vehiculo v4 = new Camion("9011BPL", 5000);
		getListaVehiculos().add(v1);
		getListaVehiculos().add(v2);
		getListaVehiculos().add(v3);
		getListaVehiculos().add(v4);
	}

	private void obtenerPrecios(int dias) {
		for (Vehiculo vehiculo : getListaVehiculos()) {
			System.out.println(vehiculo.getMatricula() + " " + vehiculo.getPrecioAlquiler(dias));
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Programa p = new Programa();
		p.crearVehiculos();	
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		try {
			System.out.println("Seleciona el numero de dias para alquilar vehiculos");
			p.obtenerPrecios(Integer.parseInt(br.readLine()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
