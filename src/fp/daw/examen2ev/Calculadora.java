package fp.daw.examen2ev;

import java.awt.font.NumericShaper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Calculadora {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String linea;
		System.out.println("calculadora > ");
		linea = in.readLine();
				
		while (!(linea.equalsIgnoreCase("fin"))) {
			calcular(linea);
			System.out.println("calculadora > ");
			linea = in.readLine();
		}
	}
		
		static void calcular(String linea) {
			Scanner s = new Scanner(linea);
			double n1 = 0;
			double n2 = 0;
			String operador;
			double resultado = 0;
			String[] lineaSplit = linea.split(" ");
			
			try {
				n1 = Double.parseDouble(lineaSplit[0]);
				n2 = Double.parseDouble(lineaSplit[2]);
			} catch (NumberFormatException e) {
				System.out.println("expresion incorrecta");
			}
			
			try {
				operador = lineaSplit[1];
				switch (operador) {
				case "*":
					System.out.println("Multiplicar");
					resultado = n1 * n2;
					break;
				case "/":
					System.out.println("Dividir");
					resultado = n1 / n2;
					break;
				case "+":
					System.out.println("Sumar");
					resultado = n1 + n2;
					break;
				case "-":
					System.out.println("Restar");
					resultado = n1 - n2;
					break;
				default:
					System.out.println("default");
					throw new Exception("expresion incorrecta");
				}
			} catch (Exception e) {
				System.out.println(e);
			}
			
			System.out.println("El resultado es: " + resultado);
						
			s.close();
	}
}
