package fp.daw.examen2ev;

public class Camion extends TransporteMercancias implements IPagable {


	public Camion(String matricula, int pma) {
		super(matricula, pma);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getPrecioAlquiler(int dias) {
		// TODO Auto-generated method stub
		return (dias * (this.getPrecioBase() + (this.getPma() * 20) ) ) + 40;
	}

	@Override
	public String toString() {
		return "Camion [getPma()=" + getPma() + ", getMatricula()=" + getMatricula() + ", getPrecioBase()="
				+ getPrecioBase() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + "]";
	}

}
