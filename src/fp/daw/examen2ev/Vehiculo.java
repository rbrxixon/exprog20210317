package fp.daw.examen2ev;

public abstract class Vehiculo implements IPagable {

	private String matricula;
	private int precioBase;
		
	public Vehiculo(String matricula) {
		this.matricula = matricula;
		this.precioBase = 50;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}


	public int getPrecioBase() {
		return precioBase;
	}


	public void setPrecioBase(int precioBase) {
		this.precioBase = precioBase;
	}
	

	@Override
	public String toString() {
		return "Vehiculo [matricula=" + matricula + ", precioBase=" + precioBase + "]";
	}
		
}
